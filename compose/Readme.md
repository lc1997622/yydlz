![docker-compose](https://img.shields.io/badge/docker--compose-1.20.0-brightgreen)

## docker-compose打包

### 快捷命令

删除已构建的镜像和容器，重新构建镜像并启动
```
make 
```
删除已构建的镜像和容器
```
make delete
```

重新构建并启动
```
make run
```
