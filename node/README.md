![node](https://img.shields.io/badge/node-v16.13.0-brightgreen)

##  前端服务器部分

前端文件放在static文件夹中

### 0、安装nodejs

```
yum install nodejs –y
```

### 1、运行

```
//安装依赖包
npm install

//运行
npm start
```

