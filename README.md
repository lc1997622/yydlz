![jdk](https://img.shields.io/badge/jdk-11%2B-brightgreen)
![docker](https://img.shields.io/badge/docker-20.10.6%2B-brightgreen)
![node](https://img.shields.io/badge/node-v16.13.0-brightgreen)
# 礼物商品秒杀平台


## 架构图

## 工程结构

```
YYDLZ
├── apis -- 微服务提供的api
├    ├── user --用户模块
├    ├── order-consumer --订单消费者模块
├    ├── order-publisher --订单生产者模块
├── compose -- docker一键部署
├── database -- 数据库及缓存
├── node -- 服务器前端
├    ├── static -- 静态资源
├    ├    ├── css -- css文件 
├    ├    ├── html -- html文件 
├    ├    ├── img -- 图片文件 
└──  └──  └── js -- js文件 
```

## 技术文档


## 界面预览