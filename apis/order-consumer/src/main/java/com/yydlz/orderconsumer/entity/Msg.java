package com.yydlz.orderconsumer.entity;

import java.util.Map;

import lombok.Data;

@Data
public class Msg {
    int status;
    String msg;
    Map<String, Object> data;
}
