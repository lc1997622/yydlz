package com.yydlz.orderconsumer.service;

import com.alibaba.fastjson.JSONObject;
import com.yydlz.orderconsumer.dao.GiftDao;
import com.yydlz.orderconsumer.dao.OrderDao;
import com.yydlz.orderconsumer.entity.Msg;
import com.yydlz.orderconsumer.entity.Order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    @Autowired
    GiftDao giftDao;

    @Autowired
    OrderDao orderDao;

    
    public Msg subOrder(JSONObject argObject){
        Msg msg = new Msg();
        System.out.println(argObject);
        msg = checkOrder(argObject);
        if(msg.getStatus() == 200){
            Order order = new Order();
            order.setId(argObject.get("order_id").toString());
            order.setGiftId(argObject.get("gift_id").toString());
            order.setUserId(argObject.get("user_id").toString());
            order.setSubTime(argObject.get("sub_time").toString());
            order.setUid(argObject.get("uid").toString());
            
            int giftnum = giftDao.giftNum(argObject.get("gift_id").toString());
            msg.setMsg("礼物已被抢完");

            int isset = 0;
            if(giftnum > 0){
                giftnum -=1;
                isset = giftDao.setGiftNum(argObject.get("gift_id").toString());
                msg.setMsg("礼物已抢成功");
            }

            order.setIsSuccess(isset);
            orderDao.insertOrder(order);
        }
        
        return msg;
    }

    private Msg checkOrder(JSONObject argObject){
        Msg msg = new Msg();
        msg.setStatus(200);
        return msg;
    }

}
