package com.yydlz.orderconsumer.dao;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GiftDao {

    int giftNum(String giftId);

    int setGiftNum(String giftId);
    
}
