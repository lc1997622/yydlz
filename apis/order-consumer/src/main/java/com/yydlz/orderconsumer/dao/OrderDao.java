package com.yydlz.orderconsumer.dao;

import java.util.List;
import java.util.Map;

import com.yydlz.orderconsumer.entity.Order;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderDao {
    public int insertOrder(Order order);
    
}
