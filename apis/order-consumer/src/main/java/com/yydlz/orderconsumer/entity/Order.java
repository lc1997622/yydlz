package com.yydlz.orderconsumer.entity;

import lombok.Data;

@Data
public class Order {
    String id;
    String userId;
    String giftId;
    String subTime;
    int isSuccess;
    String uid;
}
