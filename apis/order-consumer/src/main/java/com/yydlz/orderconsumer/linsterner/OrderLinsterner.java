package com.yydlz.orderconsumer.linsterner;

import java.io.UnsupportedEncodingException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yydlz.orderconsumer.service.OrderService;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderLinsterner {
    
    @Autowired
    OrderService orderService;

    @RabbitListener(queues = "hello")
    public void msg(Message orderInfo) throws UnsupportedEncodingException{
    
        byte[] body = orderInfo.getBody();
        String string = new String(body, "UTF-8");
        JSONObject jsonObject =  JSON.parseObject(string);
        orderService.subOrder(jsonObject);

    }
}
