package com.yydlz.users.service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

public interface JwtService {
    String getJwt(String userId) throws UnsupportedEncodingException, NoSuchAlgorithmException;
}
