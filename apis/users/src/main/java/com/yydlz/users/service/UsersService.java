package com.yydlz.users.service;

import java.util.Map;

public interface UsersService {
    String getAccessToken();

    String getUserId(String code);

    Map<String,Object> getUserInfo(String userId);

    String Oauth2API(String redirectUri);
}
