package com.yydlz.users.service.impl;

import com.yydlz.users.service.JwtService;
import com.yydlz.users.utils.JWTUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

@Service("jwtServicer")
@Transactional
public class JwtServicerImpl implements JwtService {

    @Override
    public String getJwt(String userId) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        //将相关信息放到map里面
        Map<String, String> map = new HashMap<>();
        map.put("userId", userId);//用户表id

        return JWTUtils.getToken(map);
    }

}
