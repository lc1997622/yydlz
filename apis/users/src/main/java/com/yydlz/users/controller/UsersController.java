package com.yydlz.users.controller;


import com.auth0.jwt.interfaces.Claim;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yydlz.users.service.UsersService;
import com.yydlz.users.utils.JWTUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("usersapi")
@Api(tags = "用户模块",description = "用户相关模块接口")
public class UsersController {
    @Autowired
    private UsersService usersService;


    @GetMapping("/getAccessToken")
    @ApiOperation(value = "获取AccessToken")
    public String getAccessToken(){
        return usersService.getAccessToken();
    }

    @ApiIgnore
    @PostMapping("/Oauth2API")
    public String Oauth2API(@RequestBody Map<String,Object> map ){
        String url = (String) map.get("url");
        return usersService.Oauth2API(url);
    }

    @PostMapping("/getUserId")
    @ApiOperation(value = "获取用户id")
//    @ApiImplicitParam(name = "code", value = "code", required = true, dataType = "String", paramType = "query")
    public Map<String,String> getUserId(@RequestBody Map<String,Object> map , HttpServletResponse response) throws IOException {
        String code = (String) map.get("code");


        //获取userId
        String userId = usersService.getUserId(code);


        // 创建一个 cookie对象
        Cookie cookie = new Cookie("userId", userId);
        //设置path为根路径，否则cookie只会返给/student路径
        cookie.setPath("/");
        //设置cookie过期时间-7天
        cookie.setMaxAge(7 * 24 * 60 * 60);
        //将cookie对象加入response响应
        response.addCookie(cookie);

        Map<String,String> userIdMap = new HashMap<>();
        userIdMap.put("userId",userId);
        return userIdMap;

    }

    @PostMapping("/getUserInfo")
    @ApiOperation(value = "获取用户信息")
//    @ApiImplicitParam(name = "code", value = "code", required = true, dataType = "String", paramType = "query")
    public Map<String,Object> getUserInfo(HttpServletRequest request,@RequestBody Map<String,Object> m){
        String code = (String) m.get("code");

        //获取userId
        String userId="";

        //先从cookie中读取userId
//        Cookie[] cookies = request.getCookies();
//        if(cookies != null && cookies.length > 0){
//            for (Cookie cookie : cookies){
//                if(cookie.getName().equals("token")){
//                    Claim userIdClaim = JWTUtils.verify(cookie.getValue()).getClaim("userId");
//                    userId= userIdClaim.asString();
//                }
//            }
//        }
        Cookie[] cookies = request.getCookies();
        if(cookies != null && cookies.length > 0){
            for (Cookie cookie : cookies){
                if(cookie.getName().equals("userId")){
                    userId= cookie.getValue();
                }
            }
        }

        //如果userId没在cookie中，则从接口中获取
        if(userId.equals("")){
            userId = usersService.getUserId(code);
        }

        Map<String,Object> map = usersService.getUserInfo(userId);

        return map;
    }

}
