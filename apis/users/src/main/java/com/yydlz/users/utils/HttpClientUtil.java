package com.yydlz.users.utils;


import org.springframework.web.client.RestTemplate;

import java.util.Map;

public class HttpClientUtil {

    public static Map getHttpClient(String url){
        RestTemplate restTemplate = new RestTemplate();
        Map<String,Object> map = restTemplate.getForObject(url,Map.class);

        return map;
    }
}
