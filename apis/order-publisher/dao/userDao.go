package dao

import (
	"order-publisher/model"
	"order-publisher/utils"
)

func AddUserInfo(orderInfo *model.OrderInfo) (user_id int, is_success bool, err error) {
	sqlStr := "insert into user(name, student_id, address, phone_number, uid) values(?,?,?,?,?)"
	_, err = utils.Db.Exec(sqlStr, orderInfo.Name, orderInfo.StudentID, orderInfo.Address+orderInfo.AddressDetail, orderInfo.Phone, orderInfo.UID)
	if err != nil {
		return 0, false, err
	}
	// 查询该条目对应的user表主键id
	sql := "select LAST_INSERT_ID() from user"
	row := utils.Db.QueryRow(sql)
	err = row.Scan(&user_id)
	if err != nil {
		return 0, false, err
	}
	return user_id, true, nil
}
