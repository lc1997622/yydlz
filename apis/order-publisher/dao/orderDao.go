package dao

import (
	"order-publisher/model"
	"order-publisher/utils"
)

func GetOrderByUIDAndGiftID(UID string, giftID int) (is_exist bool, err error) {
	sql := "select order.id from `order` where order.uid = ? and gift_id = ? and is_success = ?"
	is_exist = true
	row := utils.Db.QueryRow(sql, UID, giftID, 1)
	order := &model.Order{}
	err = row.Scan(&order.OrderID)
	if err != nil {
		is_exist = false
	}
	return is_exist, err
}

func GetOrderByOrderID(orderID string) (orderWithGiftAndUserInfo *model.OrderWithGiftAndUserInfo, err error) {
	sql := "select order.user_id, order.gift_id, order.sub_time, order.is_success, gift.name, gift.img, gift.info, user.name, user.student_id, user.address, user.phone_number, user.uid from `order` left join gift on order.gift_id=gift.id left join user on order.user_id=user.id where order.id = ?"
	row := utils.Db.QueryRow(sql, orderID)
	orderWithGiftAndUserInfo = &model.OrderWithGiftAndUserInfo{}
	err = row.Scan(&orderWithGiftAndUserInfo.UserID, &orderWithGiftAndUserInfo.GiftID, &orderWithGiftAndUserInfo.SubTime, &orderWithGiftAndUserInfo.IsSuccess,
		&orderWithGiftAndUserInfo.GiftName, &orderWithGiftAndUserInfo.GiftImg, &orderWithGiftAndUserInfo.GiftInfo,
		&orderWithGiftAndUserInfo.Name, &orderWithGiftAndUserInfo.StudentID, &orderWithGiftAndUserInfo.Address, &orderWithGiftAndUserInfo.Phone, &orderWithGiftAndUserInfo.UID)
	if err != nil {
		return nil, err
	}
	return orderWithGiftAndUserInfo, nil
}

func GetOrdersByUID(UID string) (orderWithGiftAndUserInfos []*model.OrderWithGiftAndUserInfo, err error) {
	sql := "select order.id, order.user_id, order.gift_id, order.sub_time, order.is_success, gift.name, gift.img, gift.info, user.id, user.name, user.student_id, user.address, user.phone_number from `order` left join gift on order.gift_id=gift.id left join user on order.user_id=user.id where order.uid = ? order by order.id"
	rows, err := utils.Db.Query(sql, UID)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		orderWithGiftAndUserInfo := &model.OrderWithGiftAndUserInfo{}
		err = rows.Scan(&orderWithGiftAndUserInfo.ID, &orderWithGiftAndUserInfo.UserID, &orderWithGiftAndUserInfo.GiftID, &orderWithGiftAndUserInfo.SubTime, &orderWithGiftAndUserInfo.IsSuccess,
			&orderWithGiftAndUserInfo.GiftName, &orderWithGiftAndUserInfo.GiftImg, &orderWithGiftAndUserInfo.GiftInfo,
			&orderWithGiftAndUserInfo.UserID, &orderWithGiftAndUserInfo.Name, &orderWithGiftAndUserInfo.StudentID, &orderWithGiftAndUserInfo.Address, &orderWithGiftAndUserInfo.Phone)
		if err != nil {
			return orderWithGiftAndUserInfos, err
		}
		orderWithGiftAndUserInfos = append(orderWithGiftAndUserInfos, orderWithGiftAndUserInfo)
	}
	return orderWithGiftAndUserInfos, nil
}

func GetOrders() (orderWithGiftAndUserInfos []*model.OrderWithGiftAndUserInfo, err error) {
	// sql := "select order.id, order.uid, order.gift_id, order.sub_time, order.is_success, gift.name, gift.img, gift.info from `order` left join gift on order.gift_id=gift.id order by order.id"
	sql := "select order.id, order.user_id, order.gift_id, order.sub_time, order.is_success, gift.name, gift.img, gift.info, user.id, user.name, user.student_id, user.address, user.phone_number, user.uid from `order` left join gift on order.gift_id=gift.id left join user on order.user_id=user.id order by order.id"
	rows, err := utils.Db.Query(sql)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		orderWithGiftAndUserInfo := &model.OrderWithGiftAndUserInfo{}
		err = rows.Scan(&orderWithGiftAndUserInfo.ID, &orderWithGiftAndUserInfo.UserID, &orderWithGiftAndUserInfo.GiftID, &orderWithGiftAndUserInfo.SubTime, &orderWithGiftAndUserInfo.IsSuccess,
			&orderWithGiftAndUserInfo.GiftName, &orderWithGiftAndUserInfo.GiftImg, &orderWithGiftAndUserInfo.GiftInfo,
			&orderWithGiftAndUserInfo.UserID, &orderWithGiftAndUserInfo.Name, &orderWithGiftAndUserInfo.StudentID, &orderWithGiftAndUserInfo.Address, &orderWithGiftAndUserInfo.Phone, &orderWithGiftAndUserInfo.UID)
		if err != nil {
			return orderWithGiftAndUserInfos, err
		}
		orderWithGiftAndUserInfos = append(orderWithGiftAndUserInfos, orderWithGiftAndUserInfo)
	}
	return orderWithGiftAndUserInfos, nil
}
