package model

// 生成订单所需的数据结构
type OrderInfo struct {
	UID           string `json:"uid"`
	GiftID        int    `json:"gift_id"`
	Name          string `json:"name"`
	StudentID     string `json:"student_id"`
	Address       string `json:"address"`
	AddressDetail string `json:"address_detail"`
	Phone         string `json:"phone_number"`
}

// 检查该用户是否下过该礼物的订单所需的数据结构
type OrderCheckInfo struct {
	UID    string `json:"uid"`
	GiftID int    `json:"gift_id"`
}

// 订单数据结构
type Order struct {
	OrderID string `json:"order_id"`
	UID     string `json:"uid"`
	UserID  string `json:"user_id"` // user表的主键id
	GiftID  int    `json:"gift_id"`
	SubTime string `json:"sub_time"`
}

// 用户查询订单所需的数据结构（目前只需UID）
type UserInfo struct {
	UID string `json:"uid"`
}

// 订单查询结果的数据结构（包括订单数据及对应的礼物数据）
type OrderWithGiftAndUserInfo struct {
	// order表
	ID        string `json:"order_id"` // order_id是一个uuid
	UID       string `json:"uid"`
	GiftID    int    `json:"gift_id"`
	SubTime   string `json:"sub_time"`
	IsSuccess int    `json:"is_success"`
	// gift表
	GiftName string `json:"gift_name"`
	GiftImg  string `json:"gift_img"`
	GiftInfo string `json:"gift_info"`
	// user表
	UserID    string `json:"user_id"` // user表主键id
	Name      string `json:"user_name"`
	StudentID string `json:"student_id"`
	Address   string `json:"address"`
	Phone     string `json:"phone_number"`
}
