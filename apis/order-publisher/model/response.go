package model

import (
	"encoding/json"
	"net/http"
)

type Result struct {
	Success bool              `json:"success"`
	Code    int               `json:"status"`
	Message string            `json:"msg"`
	Data    map[string]string `json:"data"`
}

type Result2 struct {
	Success bool                   `json:"success"`
	Code    int                    `json:"status"`
	Message string                 `json:"msg"`
	Data    map[string]interface{} `json:"data"`
}

// 返回data格式为map[string]string
func Response(w http.ResponseWriter, success bool, code int, message string, data map[string]string) {
	w.Header().Set("Content-Type", "application/json")
	rst := &Result{
		Success: success,
		Code:    code,
		Message: message,
		Data:    data,
	}
	response, _ := json.Marshal(rst)
	w.Write(response)
}

// 返回data格式为map[string]interface{}
func Response2(w http.ResponseWriter, success bool, code int, message string, data map[string]interface{}) {
	w.Header().Set("Content-Type", "application/json")
	rst := &Result2{
		Success: success,
		Code:    code,
		Message: message,
		Data:    data,
	}
	response, _ := json.Marshal(rst)
	w.Write(response)
}
