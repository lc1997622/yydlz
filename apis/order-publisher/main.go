package main

import (
	"net/http"

	"github.com/gin-gonic/gin"

	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"

	_ "order-publisher/docs"

	"order-publisher/controller"
)

// @title order-publisher API
// @version 1.0
// @description 订单生产者&订单结果查询

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host 139.224.246.43:8084
func main() {
	router := gin.Default()

	router.Use(Cors())

	// 设置 swagger 访问路由
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	router.POST("/orderapi/chooseGift", controller.ChooseGift)
	router.POST("/orderapi/orderCheck", controller.OrderCheck)
	router.POST("/orderapi/getOrderByOrderID", controller.GetOrderByOrderID)
	router.POST("/orderapi/getOrdersByUID", controller.GetOrdersByUID)
	router.GET("/orderapi//getAllOrders", controller.GetAllOrders)

	router.Run(":8084")
}

func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method
		origin := c.Request.Header.Get("Origin") //请求头部
		if origin != "" {
			c.Header("Access-Control-Allow-Origin", "*") // 可将 * 替换为指定的域名
			c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
			c.Header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
			c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Cache-Control, Content-Language, Content-Type")
			c.Header("Access-Control-Allow-Credentials", "true")
		}
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		c.Next()
	}
}
