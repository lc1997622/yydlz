package utils

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

// 模块中要导出的函数，必须首字母大写!!
var (
	Db  *sql.DB
	err error
)

const (
	// 通过socket连接
	Container_DB_Driver = "root:123456@tcp(139.224.246.43:3306)/yydlzdb"
	// 通过别名连接
	// Container_DB_Driver = "root:123456@tcp(yydlzdb)/yydlzdb"
)

// go的init()函数，先于main执行，无参数，不可被调用
func init() {
	Db, err = sql.Open("mysql", Container_DB_Driver) // 容器数据库combine

	if err != nil {
		panic(err.Error())
	}
}
