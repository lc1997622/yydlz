package com.yydlz.gift.dao;

import com.yydlz.gift.entity.Gift;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Gift)表数据库访问层
 *
 * @author makejava
 * @since 2021-12-15 12:38:30
 */
@Mapper
public interface GiftDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Gift queryById(Integer id);

    /**
     * 分页查询
     *
     * @param gift        筛选条件
     * @param pageNum
     * @param pageSize
     * @return 查询结果
     */
    List<Gift> queryAllByLimit(/*Gift gift*/@Param("pageNum") int pageNum, @Param("pageSize") int pageSize);

    /**
     * 统计总行数
     *
     * @param gift 查询条件
     * @return 总行数
     */
    long count(Gift gift);

    /**
     * 新增数据
     *
     * @param gift 实例对象
     * @return 影响行数
     */
    int insert(Gift gift);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Gift> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<Gift> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Gift> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<Gift> entities);

    /**
     * 修改数据
     *
     * @param gift 实例对象
     * @return 影响行数
     */
    int update(Gift gift);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}

