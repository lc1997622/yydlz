package com.yydlz.gift.service;

import com.yydlz.gift.entity.Gift;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

/**
 * (Gift)表服务接口
 *
 * @author makejava
 * @since 2021-12-15 12:38:30
 */
public interface GiftService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Gift queryById(Integer id);

    /**
     * 分页查询
     *
     * @param gift        筛选条件
     * @param pageNum
     * @param pageSize
     * @return 查询结果
     */
    List<Gift> queryByPage(/*Gift gift*/int pageNum, int pageSize);


    /**
     * 统计总行数
     *
     * @param gift 查询条件
     * @return 总行数
     */
    long count(Gift gift);

    /**
     * 新增数据
     *
     * @param gift 实例对象
     * @return 实例对象
     */
    Gift insert(Gift gift);

    /**
     * 修改数据
     *
     * @param gift 实例对象
     * @return 实例对象
     */
    Gift update(Gift gift);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

}
