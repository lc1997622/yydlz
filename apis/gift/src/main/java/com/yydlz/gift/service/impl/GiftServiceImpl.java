package com.yydlz.gift.service.impl;

import com.yydlz.gift.entity.Gift;
import com.yydlz.gift.dao.GiftDao;
import com.yydlz.gift.service.GiftService;
import com.yydlz.gift.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * (Gift)表服务实现类
 *
 * @author makejava
 * @since 2021-12-15 12:38:30
 */
@Service("giftService")
public class GiftServiceImpl implements GiftService {
    @Resource
    private GiftDao giftDao;

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Gift queryById(Integer id) {
        return this.giftDao.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param gift        筛选条件
     * @param pageNum
     * @param pageSize
     * @return 查询结果
     */
    @Override
    public List<Gift> queryByPage(/*Gift gift*/int pageNum,int pageSize) {

        List<Gift> giftList;

//        //先查redis
//        if(redisUtil.hasKey("giftList")){
//            giftList = (List<Gift>) redisUtil.get("giftList");
//            System.out.println("通过缓存输出");
//        }
//        //再查数据库
//        else{
//            giftList = this.giftDao.queryAllByLimit(/*gift*/pageNum,pageSize);
//
//            //存储redis
//            redisUtil.set("giftList",giftList);
//            redisUtil.expire("giftList",60*3);
//        }

        giftList = this.giftDao.queryAllByLimit(/*gift*/pageNum,pageSize);

        return giftList;
    }


    /**
     * 统计总行数
     *
     * @param gift 查询条件
     * @return 总行数
     */
    @Override
    public long count(Gift gift){
        return this.giftDao.count(gift);
    }

    /**
     * 新增数据
     *
     * @param gift 实例对象
     * @return 实例对象
     */
    @Override
    public Gift insert(Gift gift) {
        this.giftDao.insert(gift);
        return gift;
    }

    /**
     * 修改数据
     *
     * @param gift 实例对象
     * @return 实例对象
     */
    @Override
    public Gift update(Gift gift) {
        this.giftDao.update(gift);
        return this.queryById(gift.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.giftDao.deleteById(id) > 0;
    }
}
