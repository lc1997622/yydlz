package com.yydlz.gift;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableOpenApi
public class GiftApplication {

	public static void main(String[] args) {
		SpringApplication.run(GiftApplication.class, args);
		System.out.println("hello gift");
	}

}
