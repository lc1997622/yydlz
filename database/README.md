### 数据

### mysql数据库
```
//ip:port:database
139.224.246.43:3306:yydlzdb

//user:password
root:123456
```

### redis
```
//ip:port
139.224.246.43:6379

//password
123456
```

### rabbitmq

```
//ip:port
139.224.246.43:5672

//user:password
root:123456
```