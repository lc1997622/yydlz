/*
 Navicat Premium Data Transfer

 Source Server         : 阿里云
 Source Server Type    : MySQL
 Source Server Version : 50736
 Source Host           : 139.224.246.43:3306
 Source Schema         : itstudy

 Target Server Type    : MySQL
 Target Server Version : 50736
 File Encoding         : 65001

 Date: 14/11/2021 18:03:58
*/

-- 建库
CREATE DATABASE IF NOT EXISTS itstudy default charset utf8 COLLATE utf8_general_ci;

-- 切换数据库
use itstudy;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for building
-- ----------------------------
DROP TABLE IF EXISTS `building`;
CREATE TABLE `building`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `building_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of building
-- ----------------------------
INSERT INTO `building` VALUES (1, '9号楼');
INSERT INTO `building` VALUES (2, '5号楼');
INSERT INTO `building` VALUES (3, '6号楼');

-- ----------------------------
-- Table structure for dorm
-- ----------------------------
DROP TABLE IF EXISTS `dorm`;
CREATE TABLE `dorm`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `building` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `floor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dorm
-- ----------------------------
INSERT INTO `dorm` VALUES (1, '5', '1', '501');
INSERT INTO `dorm` VALUES (2, '9', '3', '9302');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `submit_uid` bigint(20) NOT NULL,
  `building_id` int(11) NOT NULL,
  `gender` int(11) NOT NULL,
  `people_num` int(11) NULL DEFAULT NULL,
  `sucess` int(11) NULL DEFAULT NULL,
  `submit_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES (5, 16, 2, 0, 1, 1, '2021-11-13 00:00:00');

-- ----------------------------
-- Table structure for order_info
-- ----------------------------
DROP TABLE IF EXISTS `order_info`;
CREATE TABLE `order_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cp_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of order_info
-- ----------------------------
INSERT INTO `order_info` VALUES (3, 16, 5);

-- ----------------------------
-- Table structure for room
-- ----------------------------
DROP TABLE IF EXISTS `room`;
CREATE TABLE `room`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_id` int(11) NOT NULL,
  `room_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gender` int(11) NOT NULL,
  `total_number` int(11) NOT NULL,
  `free_number` int(11) NOT NULL,
  `bad_number` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of room
-- ----------------------------
INSERT INTO `room` VALUES (1, 1, '9132', 0, 4, 4, 0);
INSERT INTO `room` VALUES (2, 1, '9131', 0, 4, 3, 0);
INSERT INTO `room` VALUES (3, 1, '9133', 0, 4, 2, 0);
INSERT INTO `room` VALUES (4, 1, '9135', 0, 4, 4, 0);
INSERT INTO `room` VALUES (5, 2, '9232', 0, 4, 3, 0);
INSERT INTO `room` VALUES (6, 3, '5101', 0, 4, 0, 0);
INSERT INTO `room` VALUES (7, 3, '5102', 0, 4, 2, 0);
INSERT INTO `room` VALUES (8, 3, '5103', 0, 4, 4, 0);
INSERT INTO `room` VALUES (9, 4, '5201', 1, 4, 4, 0);
INSERT INTO `room` VALUES (10, 4, '5202', 1, 4, 4, 0);
INSERT INTO `room` VALUES (11, 4, '5203', 1, 6, 2, 0);
INSERT INTO `room` VALUES (12, 6, '6101', 0, 6, 6, 0);

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `login_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_del` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `lc`(`uid`) USING BTREE,
  CONSTRAINT `lc` FOREIGN KEY (`uid`) REFERENCES `student_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (16, 16, 'AutumnLoveSpring', '44642e87faf8b129901fe7ead3534cc6', 0);

-- ----------------------------
-- Table structure for student_attest
-- ----------------------------
DROP TABLE IF EXISTS `student_attest`;
CREATE TABLE `student_attest`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) UNSIGNED NOT NULL,
  `attest_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`uid`) USING BTREE,
  CONSTRAINT `user_id` FOREIGN KEY (`uid`) REFERENCES `student_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of student_attest
-- ----------------------------
INSERT INTO `student_attest` VALUES (5, 16, 'asdwqAsd12_qS');

-- ----------------------------
-- Table structure for student_info
-- ----------------------------
DROP TABLE IF EXISTS `student_info`;
CREATE TABLE `student_info`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户表id',
  `student_id` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学号',
  `name` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
  `gender` int(11) NOT NULL COMMENT '性别',
  `email` char(55) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of student_info
-- ----------------------------
INSERT INTO `student_info` VALUES (16, '2101210251', '李超', 0, '2510834510@qq.com');

-- ----------------------------
-- Table structure for student_room
-- ----------------------------
DROP TABLE IF EXISTS `student_room`;
CREATE TABLE `student_room`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `room_id` int(11) NOT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of student_room
-- ----------------------------
INSERT INTO `student_room` VALUES (2, 16, 7, '2021-11-13 00:00:00', 0);

-- ----------------------------
-- Table structure for unit
-- ----------------------------
DROP TABLE IF EXISTS `unit`;
CREATE TABLE `unit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `building_id` int(11) NOT NULL,
  `unit_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of unit
-- ----------------------------
INSERT INTO `unit` VALUES (1, 1, '一单元');
INSERT INTO `unit` VALUES (2, 1, '二单元');
INSERT INTO `unit` VALUES (3, 2, '一单元');
INSERT INTO `unit` VALUES (4, 2, '二单元');
INSERT INTO `unit` VALUES (5, 2, '三单元');
INSERT INTO `unit` VALUES (6, 3, '一单元');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `phonenumber` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '11111111111', 'lll', '111');
INSERT INTO `user` VALUES (3, '18811111111', 'lichao123', 'b841ca531702b5f26e0cf1ee5b7e78c9');
INSERT INTO `user` VALUES (4, '18811192202', 'lichao', '44642e87faf8b129901fe7ead3534cc6');
INSERT INTO `user` VALUES (5, '18811192203', 'lichao', '44642e87faf8b129901fe7ead3534cc6');
INSERT INTO `user` VALUES (7, '18811192204', 'lichao', '44642e87faf8b129901fe7ead3534cc6');

SET FOREIGN_KEY_CHECKS = 1;
